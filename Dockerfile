FROM ruby:2.6.8-slim-bullseye AS builder

LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG DEBIAN_FRONTEND=noninteractive
ENV GOSU_VERSION 1.11
COPY keys.asc /tmp/keys.asc
RUN set -ex; \
  apt-get update; \
  apt-get install -y --no-install-recommends gnupg2 dirmngr build-essential curl git libimlib2-dev libpq-dev patch shared-mime-info nodejs libclang-dev clang llvm pkg-config nettle-dev rustc cargo libmariadb-dev; \
  gpg2 --import /tmp/keys.asc ; \
  rm /tmp/keys.asc ; \
  gpgconf --kill all ; \
  rm -rf /var/lib/apt/lists/* ; \
  curl -s -J -L -o /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$(dpkg --print-architecture)" ; \
  curl -s -J -L -o /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/${GOSU_VERSION}/gosu-$(dpkg --print-architecture).asc" ; \
  gpg2 --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu ; \
  rm /usr/local/bin/gosu.asc ; \
  chmod +x /usr/local/bin/gosu ; \
  gosu nobody true

COPY package-auto-reinstall.patch  /tmp/package-auto-reinstall.patch
COPY fetch_locales.rb  /tmp/fetch_locales.rb

ARG SEQUOIA_PROJECT_URL=https://gitlab.com/sequoia-pgp/sequoia-ffi.git
ARG SEQUOIA_GIT_TAG=main
ENV SEQUOIA_DIR=/usr/lib/sequoia
ENV LD_LIBRARY_PATH=${SEQUOIA_DIR}/target/debug
ARG ZAMMAD_PROJECT_URL=https://github.com/zammad/zammad.git
ARG ZAMMAD_GIT_TAG=develop
ENV ZAMMAD_TMP_DIR /tmp/zammad-${ZAMMAD_GIT_TAG}

ENV ZAMMAD_DIR /opt/zammad
ENV ZAMMAD_USER zammad
ENV RAILS_ENV production

RUN set -ex; \
  groupadd -g 1000 "${ZAMMAD_USER}"; \
  useradd -M -d "${ZAMMAD_DIR}" -s /bin/bash -u 1000 -g 1000 "${ZAMMAD_USER}" ; \
  git clone -b "${SEQUOIA_GIT_TAG}" --single-branch --depth 1 "${SEQUOIA_PROJECT_URL}" "${SEQUOIA_DIR}" ; \
  cd "${SEQUOIA_DIR}" && cargo build -p sequoia-openpgp-ffi ; \
  git clone -b "${ZAMMAD_GIT_TAG}" --single-branch --depth 1 "${ZAMMAD_PROJECT_URL}" "${ZAMMAD_TMP_DIR}" ; \
  cd ${ZAMMAD_TMP_DIR}; \
  echo "gem 'ruby_openpgp', git: 'https://github.com/throneless-tech/ruby_openpgp', branch: 'signing-and-userids'" >> Gemfile.local ; \
  echo "gem 'rails-observers'" >> Gemfile.local ; \
  bundle update tcr; \
  bundle install --without test development mysql ; \
  /tmp/fetch_locales.rb ; \
  sed -e 's#.*adapter: postgresql#  adapter: nulldb#g' -e 's#.*username:.*#  username: postgres#g' -e 's#.*password:.*#  password: \n  host: zammad-postgresql\n#g' < contrib/packager.io/database.yml.pkgr > config/database.yml ; \
  sed -i "/require 'rails\/all'/a require\ 'nulldb'" config/application.rb ; \
  sed -i 's/.*scheduler_\(err\|out\).log.*//g' script/scheduler.rb ; \
  touch db/schema.rb ; \
  bundle exec rake assets:precompile ; \
  chown -R "${ZAMMAD_USER}":"${ZAMMAD_USER}" "${ZAMMAD_TMP_DIR}"

COPY auto_install "${ZAMMAD_TMP_DIR}"/auto_install

FROM ruby:2.6.8-slim-bullseye

LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG BUILD_DATE
ARG DEBIAN_FRONTEND=noninteractive

LABEL org.label-schema.build-date="$BUILD_DATE" \
  org.label-schema.name="Zammad" \
  org.label-schema.license="AGPL-3.0" \
  org.label-schema.description="Docker container for Zammad - Data Container" \
  org.label-schema.url="https://zammad.org" \
  org.label-schema.vcs-url="https://github.com/zammad/zammad" \
  org.label-schema.vcs-type="Git" \
  org.label-schema.vendor="Zammad" \
  org.label-schema.schema-version="2.9.0" \
  org.label-schema.docker.cmd="sysctl -w vm.max_map_count=262144;docker-compose up"


ARG ZAMMAD_GIT_TAG=develop
ENV RAILS_ENV production
ENV SEQUOIA_DIR=/usr/lib/sequoia
ENV LD_LIBRARY_PATH=${SEQUOIA_DIR}/target/debug
ENV ZAMMAD_DIR /opt/zammad
ENV ZAMMAD_READY_FILE ${ZAMMAD_DIR}/tmp/zammad.ready
ENV ZAMMAD_TMP_DIR /tmp/zammad-${ZAMMAD_GIT_TAG}
ENV ZAMMAD_USER zammad

RUN set -ex; \
  apt-get update; \
  apt-get install -y --no-install-recommends curl libimlib2 libimlib2-dev libpq5 nginx rsync clang llvm pkg-config; \
  rm -rf /var/lib/apt/lists/*

RUN set -ex; \
  groupadd -g 1000 "${ZAMMAD_USER}" ; \
  useradd -M -d "${ZAMMAD_DIR}" -s /bin/bash -u 1000 -g 1000 "${ZAMMAD_USER}"

COPY --from=builder ${ZAMMAD_TMP_DIR} ${ZAMMAD_TMP_DIR}
COPY --from=builder ${SEQUOIA_DIR} ${SEQUOIA_DIR}
COPY --from=builder /usr/local/bin/gosu /usr/local/bin/gosu
COPY --from=builder /usr/local/bundle /usr/local/bundle

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

WORKDIR ${ZAMMAD_DIR}

ENV AUTOWIZARD_JSON=
ENV ELASTICSEARCH_HOST=zammad-elasticsearch
ENV ELASTICSEARCH_PORT=9200
ENV ELASTICSEARCH_SCHEMA=http
ENV ELASTICSEARCH_SSL_VERIFY=true
ENV ELASTICSEARCH_PURGE=false
ENV MEMCACHED_HOST=zammad-memcached
ENV MEMCACHED_PORT=11211
ENV POSTGRESQL_HOST=zammad-postgresql
ENV POSTGRESQL_PORT=5432
ENV POSTGRESQL_USER=postgres
ENV POSTGRESQL_PASS=
ENV POSTGRESQL_DB=zammad_production
ENV POSTGRESQL_DB_CREATE=true
ENV ZAMMAD_RAILSSERVER_HOST=zammad-railsserver
ENV ZAMMAD_RAILSSERVER_PORT=3000
ENV ZAMMAD_WEBSOCKET_HOST=zammad-websocket
ENV ZAMMAD_WEBSOCKET_PORT=6042
ENV NGINX_SERVER_NAME=_
ENV RAILS_SERVER puma
ENV RAILS_LOG_TO_STDOUT true
