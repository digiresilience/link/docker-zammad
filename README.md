# docker-zammad

[![pipeline status](https://gitlab.com/digiresilience/link/docker-zammad/badges/master/pipeline.svg)](https://gitlab.com/digiresilience/link/docker-zammad/-/commits/master)

Builds Link's Zammad docker container

[Zammad](https://github.com/zammad/zammad) is a web based open source helpdesk/customer support system.

This project started as a fork of the official [zammad docker](https://github.com/zammad/zammad-docker-compose) project.

It builds the [Center for Digital Resilience's version of Zammad](https://gitlab.com/digiresilience/link/zammad)

## Developer Notes

### Building

### Simple

```bash
make ZAMMAD_TAG=v3.3.0
```

Creates an image `digiresilience/zammad:v3.3.0`

### Your own tag

Supply your own docker image tag:

```bash
make ZAMMAD_TAG=v3.3.0 DOCKER_TAG=myspecialversion
```

Creates an image `digiresilience/zammad:myspecialversion`

### You special snowflake you

```bash
make ZAMMAD_TAG=develop DOCKER_TAG=myspecialversion PROJECT_URL=http://my/zammadrepo.git DOCKER_NS=batman
```

Creates the image `batman/zammad:myspecialversion` based off `develop` branch of the git repo at `http://my/zammadrepo.git`

### Help and Support

Join us in our public matrix channel [#cdr-link-dev-support:matrix.org](https://matrix.to/#/#cdr-link-dev-support:matrix.org?via=matrix.org&via=neo.keanu.im).

## Credits and License

Zammad is licensed under the [GNU Affero General Public License (AGPL)
v3+](https://www.gnu.org/licenses/agpl-3.0.en.html). So is this project.

## Maintainers

- Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)
