#!/usr/bin/env ruby
# Copyright (C) 2012-2016 Zammad Foundation, http://zammad-foundation.org/

require 'rubygems'
require 'uri'
require 'net/http'
require 'json'
require 'yaml'

version = File.read('VERSION')
version.strip!

url_locales = 'https://i18n.zammad.com/locales.json'

file_locales = "config/locales-#{version}.yml"

# download locales
uri = URI.parse(url_locales)
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
request = Net::HTTP::Get.new(uri)
response = http.request(request)
data = JSON.parse(response.body)

puts "Writing #{file_locales}..."
File.open(file_locales, 'w') do |out|
  YAML.dump(data, out)
end

puts 'done'
