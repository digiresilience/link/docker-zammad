UPSTREAM_BRANCH ?= main
UPSTREAM_JOB ?= build
ADDONS := 16838842 23149108
BUILD_DATE   ?=$(shell date -u +”%Y-%m-%dT%H:%M:%SZ”)
DOCKER_ARGS  ?=
PROJECT_URL  ?= https://github.com/zammad/zammad.git
ZAMMAD_TAG   ?= stable-4.1
DOCKER_NS    ?= registry.gitlab.com/digiresilience/link/docker-zammad
DOCKER_TAG   ?= ${ZAMMAD_TAG}
DOCKER_BUILD := docker build ${DOCKER_ARGS}
DOCKER_BUILD_FRESH := ${DOCKER_BUILD} --pull --no-cache
DOCKER_BUILD_ARGS := --build-arg PROJECT_URL=${PROJECT_URL} --build-arg ZAMMAD_GIT_TAG=${ZAMMAD_TAG} --build-arg BUILD_DATE=${BUILD_DATE}
DOCKER_PUSH  := docker push
DOCKER_BUILD_TAG := ${DOCKER_NS}:${DOCKER_TAG}

.PHONY: zammad clean env

env:
	@echo
	@echo
	@echo Build Environment
	@echo ---------------------------
	@echo "DOCKER_NS=${DOCKER_NS}"
	@echo "DOCKER_TAG=${DOCKER_TAG}"
	@echo "DOCKER_TAG_NEW=${DOCKER_TAG_NEW}"
	@echo "DOCKER_ARGS=${DOCKER_ARGS}"
	@echo "UPSTREAM_BRANCH=${UPSTREAM_BRANCH}"
	@echo "UPSTREAM_JOB=${UPSTREAM_JOB}"
	@echo "ZAMMAD_TAG=${ZAMMAD_TAG}"
	@echo "PROJECT_URL=${PROJECT_URL}"
	@echo ---------------------------
	@echo
	@echo

build: addons env
	${DOCKER_BUILD} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

build-fresh: addons env
	${DOCKER_BUILD_FRESH} ${DOCKER_BUILD_ARGS} -t ${DOCKER_BUILD_TAG} ${PWD}

push:
	${DOCKER_PUSH} ${DOCKER_BUILD_TAG}

build-push: build push
build-fresh-push: build-fresh push

add-tag: env
	docker pull ${DOCKER_NS}:${DOCKER_TAG}
	docker tag ${DOCKER_NS}:${DOCKER_TAG} ${DOCKER_NS}:${DOCKER_TAG_NEW}
	docker push ${DOCKER_NS}:${DOCKER_TAG_NEW}

keys:
	gpg2 --batch --keyserver "ha.pool.sks-keyservers.net" --recv-keys "B42F6819007F00F88E364FD4036A9C25BF357DD4"
	gpg2 --batch --keyserver "ha.pool.sks-keyservers.net" --recv-keys "9185813DDCCD789E5D4BA51B884B649C340C81F4"
	gpg2 --export --armor  "B42F6819007F00F88E364FD4036A9C25BF357DD4" > keys.asc
	gpg2 --export --armor  "9185813DDCCD789E5D4BA51B884B649C340C81F4" >> keys.asc

$(ADDONS):
	@echo Fetching addon "https://gitlab.com/api/v4/projects/$@/jobs/artifacts/${UPSTREAM_BRANCH}/download?job=${UPSTREAM_JOB}&job_token=redacted"
	@curl ${ACCESS_HEADER} -s -L "https://gitlab.com/api/v4/projects/$@/jobs/artifacts/${UPSTREAM_BRANCH}/download?job=${UPSTREAM_JOB}&job_token=${CI_JOB_TOKEN}" --output $@.zip
	unzip -o -d addons $@.zip

fetch-addons: $(ADDONS)

# note: do not use find -exec, it eats errors
install-addons:
	mkdir -p auto_install
	@find addons -type f -iname "*.szpm" -print0 | xargs -0 -I '{}' cp {} ./auto_install/
	@find ./auto_install/ -depth -name "*.szpm" -print0 | xargs -0 -I '{}' sh -c 'mv "$$1" "$${1%.szpm}.zpm"' _ {}
	@echo
	@echo
	@echo "Installed addons:"
	@find ./auto_install/ -iname "*zpm"
	@echo
	@echo

addons: fetch-addons install-addons
